<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;

use App\Http\Controllers\MenuController;

class HomeController extends Controller
{
    public $menu;

    public function __construct() {
        date_default_timezone_set('Asia/Jakarta');
        $this->menu = new MenuController;
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $data = array(
                'title'   => 'Dashboard',
                'menu'    => 'dashboard',
                'submenu' => '',
            );
        return view('dashboard');
    }

    public function start(Request $request){
        $menu = $this->menu->listMainMenu([]);
        $data = array(
                'menu' => $menu
            );
        return view('start', $data);
    }

    public function game(Request $request) {
        $menu = $this->menu->listMainMenu([]);

        // print_r($menu);
        // exit();

        $data = array(
                'menus' => $menu
            );

        return view('random', $data);
    }

    public function printData(Request $request) {
        echo 'test';
        $exec = exec('print /d:\\\\jenggot\EPSON C:\README.md');
        var_dump($exec);
        // return $exec;
    }
}
