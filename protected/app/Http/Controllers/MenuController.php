<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Lib\MyHelper;
use App\Menu;
use DB;
use Image;

use App\Http\Requests\Menu\Create;
use App\Http\Requests\Menu\Update;

class MenuController extends Controller
{
    public $saveImage;
    public $endPoint;

    public function __construct()
    {
        $this->middleware('auth');
        date_default_timezone_set('Asia/Jakarta');
        $this->saveImage = env('APP_UPLOAD_PATH');
        $this->endPoint  = env('APP_URL');
    }

    /**
     * CREATE START
     */
    // GET
    function create(Request $request) {
    	$data = array(
    	        'title'   => 'Create Menu',
    	        'menu'    => 'menu',
    	        'submenu' => 'create',
    	    );
    	return view('menu.create', $data);
    }

    // POST
    function createPost(Create $request) {
    	$post = $request->all();

		unset($post['_token']);

		$save = $this->createMenu($post);

		if ($save) {
			session(['success' => ['s' => 'Menu has been created.']]);

			return back();
		}
		else {
			return back()->withErrors(['Something went wrong. Please try again.'])->withInput();
		}
    }

    // DB
    function createMenu($post) {
    	$data = [];

    	if (isset($post['plu_id'])) {
    		$data['plu_id'] = $post['plu_id'];
    	}

    	if (isset($post['name'])) {
    		$data['name'] = $post['name'];
    	}

    	if (isset($post['prices'])) {
    		$data['prices'] = $post['prices'];
    	}

    	if (isset($post['picture'])) {
			$heigh  = MyHelper::setSizeImage($post['picture']);	
			$ext    = $post['picture']->extension();
			
			$upload = MyHelper::uploadPhotoStrict($post['picture'], $this->saveImage.'menu/', $heigh,$heigh, null, $ext);

    		if ($upload['status'] == "success") {
    			$data['picture'] = $upload['path'];
    		}
    	}

    	// create
    	$save = Menu::create($data);

    	if ($save) {
    		return true;
    	}
    	else {
    		return false;
    	}
    }

    /**
     * =====================================================================================================================
     * =====================================================================================================================
     */


    /**
     * list
     */
    // LIST
    function listMenu(Request $request) {
    	$data = array(
    	        'title'   => 'Menu',
    	        'menu'    => 'menu',
    	        'submenu' => 'list',
    	    );

    	$post = $request->all();

    	if (isset($post)) {
    		$data['listMenu'] = $this->listMainMenu($post);
    	}
    	else {
    		$data['listMenu'] = $this->listMainMenu(null);
    	}

        // print_r($data); exit();

    	return view('menu.list', $data);
    }

    // EDIT
    function selected($plu_id, Request $request) {
    	$menu = $this->listMainMenu(['plu_id' => $plu_id]);

    	if (empty($menu)) {
    		return back()->withError(['e' => 'Data '.$plu_id.' not found.']);
    	}
    	else {
    		$data = array(
		        'title'   => 'Menu',
		        'menu'    => 'menu',
		        'submenu' => 'list',
		    );

		    $data['listMenu'] = $menu;

		    return view('menu.edit', $data);
    	}
    }

    // DB
    function listMainMenu($post=null) {

    	$menu = Menu::select(
    			'id',
    			'plu_id',
    			'name',
    			'prices',
    			'picture',
    			DB::raw('if(picture != "", (select concat("'.$this->endPoint.'", picture)), "'.$this->endPoint.'image/default.png") as url_picture')
    		);

    	if (isset($post['id'])) {
            if (is_array($post['id'])) {
                $menu->whereIn('id', $post['id']);
            }
            else {
    		  $menu->where('id', $post['id']);
            }
    	}

    	if (isset($post['plu_id'])) {
    		$menu->where('plu_id', $post['plu_id']);
    	}

    	$menu = $menu->get()->toArray();

    	return $menu;
    }


    /**
     * =====================================================================================================================
     * =====================================================================================================================
     */
   

    /**
     * Update Main
     */
    // UPDATE
    function update($plu_id, Update $request) {
    	$post = $request->all();
    	unset($post['_token']);

    	$key = $post['id'];

    	$update = $this->updateMenu($key, $post);

    	if ($update) {
    		session(['success' => ['s' => 'Menu has been updated.']]);

    		return redirect('admin/menu');
    	}
    	else {
    		return back()->withErrors(['Something went wrong. Please try again.'])->withInput();	
    	}
    	
    }

    // DB
    function updateMenu($key, $post) {
    	$data = [];

    	if (isset($post['plu_id'])) {
    		$data['plu_id'] = $post['plu_id'];
    	}

    	if (isset($post['name'])) {
    		$data['name'] = $post['name'];
    	}

    	if (isset($post['prices'])) {
    		$data['prices'] = $post['prices'];
    	}

    	if (isset($post['picture'])) {

    		// delete picture
    		$info = Menu::where('id', $key)->get()->toArray();

    		if (!empty($info)) {
    			if (!empty($info[0]['picture'])) {
    				MyHelper::deletePhoto($info[0]['picture']);
    			}
    		}

    		$heigh  = MyHelper::setSizeImage($post['picture']);	

            // print_r($heigh); exit();
    		$ext    = $post['picture']->extension();
    		
    		$upload = MyHelper::uploadPhotoKotak($post['picture'], $this->saveImage.'menu/', $heigh,$heigh, null, $ext);

    		if ($upload['status'] == "success") {
    			$data['picture'] = $upload['path'];
    		}
    	}

    	// update
    	$update = Menu::where('id', $key)->update($data);

    	if ($update) {
    		return true;
    	}
    	else {
    		return false;
    	}
    }

     /**
     * =====================================================================================================================
     * =====================================================================================================================
     */

    /**
     * Delete Main
     */
    // DELETE
    function delete(Request $request) {
        $post = $request->all();

        if ($this->deleteMenu($post['id'])) {
            $result = "yes";
        }
        else {
            $result = "no";
        }

        return $result;
    }

    // DB
    function deleteMenu($key) {
    	// delete picture
    	$info = Menu::where('id', $key)->get()->toArray();

    	if (!empty($info)) {
    		if (!empty($info[0]['picture'])) {
    			MyHelper::deletePhoto($info[0]['picture']);
    		}
    	}

    	// delete data
    	$delete = Menu::where('id', $key)->delete();

    	if ($delete) {
    		return true;
    	}
    	else {
    		return false;
    	}
    }
}
