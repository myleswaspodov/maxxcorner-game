<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Lib\MyHelper;
use App\Menu;
use DB;
use Image;

class MenuFromExcelController extends Controller
{
	public function __construct()
	{
	    // $this->middleware('auth');
	    date_default_timezone_set('Asia/Jakarta');
	    $this->saveImage = env('APP_UPLOAD_PATH');
	    $this->endPoint  = env('APP_URL');
	}

	public function import(Request $request) {
		$menu = [];

		/**
		 * Sheet 1
		 */

		$menuMaxSheetSatu = array(
			'ESPRESSO COFFEE' => array('HOT' => '', 'ICE' => ''),
			'SINGLE ESPRESSO' => array('HOT' => 'IDR 12,000.00', 'ICE' => ''),
			'DOUBLE ESPRESSO' => array('HOT' => 'IDR 14,000.00', 'ICE' => ''),
			'LONG BLACK' => array('HOT' => 'IDR 14,000.00', 'ICE' => 'IDR 14,000.00'),
			'CARAMEL MACHIATTO' => array('HOT' => 'IDR 24,000.00', 'ICE' => 'IDR 24,000.00'),
			'LATTE' => array('HOT' => 'IDR 19,000.00', 'ICE' => 'IDR 19,000.00'),
			'CAPPUCINO' => array('HOT' => 'IDR 19,000.00', 'ICE' => 'IDR 19,000.00'),
			'MILO MOCHA' => array('HOT' => 'IDR 24,000.00', 'ICE' => 'IDR 24,000.00'),
			'MELAKA LATTE' => array('HOT' => 'IDR 24,000.00', 'ICE' => 'IDR 24,000.00'),
			'FLAVOUR LATTE' => array('HOT' => 'IDR 24,000.00', 'ICE' => 'IDR 24,000.00'),
			'TRADITIONAL COFFEE' => array('HOT' => '', 'ICE' => ''),
			'COFFEE TUBRUK ROBUSTA' => array('HOT' => 'IDR 10,000.00', 'ICE' => ''),
			'COFFEE TUBRUK ARABICA' => array('HOT' => 'IDR 12,000.00', 'ICE' => ''),
			'KOPI SUSU' => array('HOT' => 'IDR 17,000.00', 'ICE' => 'IDR 17,000.00'),
			'SAIGON DRIP' => array('HOT' => 'IDR 17,000.00', 'ICE' => 'IDR 17,000.00'),
			'ICE COFFEE WITH COFFEE JELLY' => array('HOT' => '', 'ICE' => 'IDR 22,000.00'),
			'DESSERT LATTE' => array('HOT' => '', 'ICE' => ''),
			'MATCHA GREEN TEA LATTE' => array('HOT' => 'IDR 24,000.00', 'ICE' => 'IDR 24,000.00'),
			'MILO' => array('HOT' => 'IDR 24,000.00', 'ICE' => 'IDR 24,000.00'),
			'TEA' => array('HOT' => '', 'ICE' => ''),
			'BLACK TEA' => array('HOT' => 'IDR 14,000.00', 'ICE' => 'IDR 14,000.00'),
			'GREEN TEA' => array('HOT' => 'IDR 14,000.00', 'ICE' => 'IDR 14,000.00'),
			'ICE TEA' => array('HOT' => '', 'ICE' => ''),
			'BLUEBERRY TEA' => array('HOT' => '', 'ICE' => 'IDR 19,000.00'),
			'LYCHEE TEA WITH LYCHEE POPPING BOBBA' => array('HOT' => '', 'ICE' => 'IDR 24,000.00'),
			'PEACH TEA WITH MANGO POPPING BOBBA' => array('HOT' => '', 'ICE' => 'IDR 24,000.00'),
			'MELAKA MILK TEA' => array('HOT' => '', 'ICE' => 'IDR 24,000.00'),
			'PANDAN TEA WITH COCONUT JELLY' => array('HOT' => '', 'ICE' => 'IDR 24,000.00'),
			'GRASS JELLY MILK TEA' => array('HOT' => '', 'ICE' => 'IDR 24,000.00'),
			'KUMQUAT GREEN TEA' => array('HOT' => '', 'ICE' => 'IDR 24,000.00'),
			'LEMON TEA' => array('HOT' => '', 'ICE' => 'IDR 19,000.00'),
			'COFFEE FRAPPE' => array('HOT' => '', 'ICE' => ''),
			'COCONUT COFFEE' => array('HOT' => '', 'ICE' => 'IDR 19,000.00'),
			'AVOCADO COFFEE' => array('HOT' => '', 'ICE' => 'IDR 34,000.00'),
			'CARAMEL COFFEE' => array('HOT' => '', 'ICE' => 'IDR 34,000.00'),
			'MILO MOCHA CHIP' => array('HOT' => '', 'ICE' => 'IDR 34,000.00'),
			'CREAM FRAPPE' => array('HOT' => '', 'ICE' => ''),
			'COCONUT MILO CREAM' => array('HOT' => '', 'ICE' => 'IDR 24,000.00'),
			'AVOCADO CHOCOLATE' => array('HOT' => '', 'ICE' => 'IDR 34,000.00'),
			'MATCHA' => array('HOT' => '', 'ICE' => 'IDR 34,000.00'),
			'VANILLA' => array('HOT' => '', 'ICE' => 'IDR 34,000.00'),
			'RTD' => array('HOT' => '', 'ICE' => ''),
			'PRISTINE MINERAL WATER' => array('HOT' => '', 'ICE' => 'IDR 12,000.00'),
			'RAUCH APPLE' => array('HOT' => '', 'ICE' => 'IDR 19,000.00'),
			'RAUCH ORANGE' => array('HOT' => '', 'ICE' => 'IDR 19,000.00'),
		);

		$x = 1;
		foreach ($menuMaxSheetSatu as $key => $value) {
			if (!empty($key)) {
				
				$value = array_filter($value);

				if (!empty($value)) {
					foreach ($value as $k => $v) {
						$menuTemp = [
							'plu_id' => 'beverages'.sprintf("%04d", $x),
							'name'   => $key." ".$k,
							'prices' => (int) substr(str_replace(",", "", substr($v, 0, -3)), 3) ,
							'picture' => ""
						];
						
						array_push($menu, $menuTemp);
						
						$x++;
					}
				}
			}
		}

		/**
		 * Sheet 2
		 */

		$menuMaxSheetDua = array(
			'TOASTED SANDWICHES' => array(''),
			'White Toast w/ 1 Basic Filling' => array('18,000'),
			'White Toast w/ 1 Basic Filling, 1 Topping' => array('21,000'),
			'White Toast w/ 1 Basic Filling, 2 Topping' => array('24,000'),
			'White Toast w/ 2 Basic Filling, 1 Topping' => array('27,000'),
			'White Toast w/ 2 Basic Filling, 2 Topping' => array('30,000'),
			'Whole Wheat Toast w/ 1 Basic Filling' => array('18,000'),
			'Whole Toast w/ 1 Basic Filling, 1 Topping' => array('21,000'),
			'Whole Toast w/ 1 Basic Filling, 2 Topping' => array('24,000'),
			'Whole Toast w/ 2 Basic Filling, 1 Topping' => array('27,000'),
			'Whole Toast w/ 2 Basic Filling, 2 Topping' => array('30,000'),
			'Basic Fillings' => array(''),
			'Nutella Spread' => array('3,000'),
			'Strawberry Jam' => array('3,000'),
			'Peanut Butter' => array('3,000'),
			'Kaya Spread' => array('3,000'),
			'Chocolate Spread' => array('3,000'),
			'' => array(''),
			'Toppings' => array(''),
			'White Chocolate' => array('3,000'),
			'Chocolate Meises' => array('3,000'),
			'Banana' => array('3,000'),
			'Marshmallow' => array('3,000'),
			'Oreo' => array('3,000'),
			'Cheese' => array('3,000'),
			'Kaya Toast' => array('18,000'),
			'Banana Nutella' => array('21,000'),
			'Smore\'s Chocolate Sandwich' => array('21,000'),
			'Oreo & White Chocolate Sandwich' => array('24,000'),
			'TRADITIONAL' => array(''),
			'Local Offerings (Assortment)' => array('7,000'),
			'Lupis' => array('7,000'),
			'Lapis sagu Pandan' => array('5,500'),
			'Lapis Sagu Cokelat' => array('5,500'),
			'Risol Ragut' => array('5,500'),
			'Pastel Ayam' => array('5,500'),
			'Singkong Goreng' => array('16,000'),
			'LIGHTMEALS' => array(''),
			'Lasagna' => array('29,000'),
			'Macaroni Schotel' => array('29,000'),
			'Cheeseburger' => array('24,000'),
			'Hot dog' => array('24,000'),
			'PASTRIES' => array(''),
			'Plain Croissant' => array('10,000'),
			'Beef Sausage Croissant' => array('17,000'),
			'Cinnamon Roll' => array('12,000'),
			'Chocolate Croissant' => array('12,000'),
			'Cheese Croissant' => array('12,000'),
			'Chicken Curry Puff' => array('17,000'),
			'PIZZA' => array(''),
			'Spicy Chicken' => array('24,000'),
			'Italian Cabonara' => array('24,000'),
			'Chicken Terriyaki' => array('24,000'),
		);

		foreach ($menuMaxSheetDua as $key => $value) {
			
			$value = array_filter($value);

			if (!empty($value)) {
				foreach ($value as $v) {
					$menuTemp = [
						'plu_id' => 'food'.sprintf("%04d", $x),
						'name'   => $key,
						'prices' => (int) str_replace(",", "", $v),
						'picture' => ""
					];

					array_push($menu, $menuTemp);

					$x++;
				}
			}
		}

		$cek = Menu::get()->toArray();

		if (empty($cek)) {
			$save = Menu::insert($menu);

			if ($save) {
				$result = [
					'status' => 'success',
					'message' => 'Menu has been imported.'
				];
			}
			else {
				$result = [
					'status' => 'fail'
				];
			}
		}
		else {
			$result = [
				'status' => 'success',
				'message' => 'Already imported.'
			];
		}

		// return $menu;
		// 
		return $result;
	}    
}
