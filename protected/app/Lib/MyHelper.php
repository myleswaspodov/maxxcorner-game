<?php
namespace App\Lib;

use Image;
use File;
use DB;

class MyHelper{

  public static function  createrandom($digit) {
    // $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    srand((double)microtime()*1000000);
    $i = 0;
    $pin = '';

    while ($i < $digit) {
      $num = rand() % strlen($chars);
      $tmp = substr($chars, $num, 1);
      $pin = $pin . $tmp;
      $i++;
      // supaya char yg sudah tergenerate tidak akan dipakai lagi
      $chars = str_replace($tmp, "", $chars);
    }

    return $pin;
  }
  
  public static function throwError($e){
      $error = $e->getFile().' line '.$e->getLine();
      $error = explode('\\', $error);
      $error = end($error);
      return ['status' => 'failed with exception', 'exception' => get_class($e),'error' => $error ,'message' => $e->getMessage()];
  }

  public static function checkExtensionImageBase64($imgdata){
       $f = finfo_open();
       $imagetype = finfo_buffer($f, $imgdata, FILEINFO_MIME_TYPE);

       if(empty($imagetype)) return '.jpg';
       switch($imagetype)
       {
          case 'image/bmp': return '.bmp';
          case 'image/cis-cod': return '.cod';
          case 'image/gif': return '.gif';
          case 'image/ief': return '.ief';
          case 'image/jpeg': return '.jpg';
          case 'image/pipeg': return '.jfif';
          case 'image/tiff': return '.tif';
          case 'image/x-cmu-raster': return '.ras';
          case 'image/x-cmx': return '.cmx';
          case 'image/x-icon': return '.ico';
          case 'image/x-portable-anymap': return '.pnm';
          case 'image/x-portable-bitmap': return '.pbm';
          case 'image/x-portable-graymap': return '.pgm';
          case 'image/x-portable-pixmap': return '.ppm';
          case 'image/x-rgb': return '.rgb';
          case 'image/x-xbitmap': return '.xbm';
          case 'image/x-xpixmap': return '.xpm';
          case 'image/x-xwindowdump': return '.xwd';
          case 'image/png': return '.png';
          case 'image/x-jps': return '.jps';
          case 'image/x-freehand': return '.fh';
          default: return false;
       }
  }

  public static function setSizeImage($foto) {
    
    $img     = Image::make($foto);
    
    // cek resolusi
    $width   = $img->width();
    $height  = $img->height();

    $perbandingan = $width / $height;

    if ($perbandingan != 1) {
      if ($height > 500) {
        $photo = $height;
      }
      else {
        $photo = $height;
      }
    }
    else {
      if ($height > 500) {
        $photo = 500;
      }
      else {
        $photo = $width;
      }
    }

    return $photo;
  }

  

  public static function uploadPhotoStrict($foto, $path, $width=1000, $height=1000, $name=null, $ext) {
    // set picture name
    if($name != null)
      $pictName = $name.$ext;
    else
      $pictName = mt_rand(0, 1000).''.time().'.'.$ext;
    
    // path
    $upload = $path.$pictName;

    $img = Image::make($foto);

    $imgwidth = $img->width();
    $imgheight = $img->height();

    if($imgwidth < $imgheight){
      //potrait
      if($imgwidth < $width){
        $img->resize($width, null, function ($constraint) {
          $constraint->aspectRatio();
          $constraint->upsize();
        });
      }
      
      if($imgwidth > $width){
        $img->resize($width, null, function ($constraint) {
          $constraint->aspectRatio();
        });
      }
    } else {
      //landscape
      if($imgheight < $height){
        $img->resize(null, $height, function ($constraint) {
          $constraint->aspectRatio();
          $constraint->upsize();
        });
      }
      if($imgheight > $height){
        $img->resize(null, $height, function ($constraint) {
          $constraint->aspectRatio();
        });
      }
    }
    
    $img->crop($width, $height);

    if ($img->save($upload)) {
        $result = [
          'status' => 'success',
          'path'  => $upload
        ];
    }
    else {
      $result = [
        'status' => 'fail'
      ];
    }  

    return $result;
  }

  public static function uploadPhotoKotak($foto, $path, $width=1000, $height=1000, $name=null, $ext) {
    // set picture name
    if($name != null)
      $pictName = $name.$ext;
    else
      $pictName = mt_rand(0, 1000).''.time().'.'.$ext;
    
    // path
    $upload = $path.$pictName;

    $img = Image::make($foto);

    $imgwidth = $img->width();
    $imgheight = $img->height();

    if($imgwidth < $imgheight){
      //potrait
      if($imgwidth < $width){
        $img->resize($width, null, function ($constraint) {
          $constraint->aspectRatio();
          $constraint->upsize();
        });
      }
      
      if($imgwidth > $width){
        $img->resize($width, null, function ($constraint) {
          $constraint->aspectRatio();
        });
      }
    } else {
      //landscape
      if($imgheight < $height){
        $img->resize(null, $height, function ($constraint) {
          $constraint->aspectRatio();
          $constraint->upsize();
        });
      }
      if($imgheight > $height){
        $img->resize(null, $height, function ($constraint) {
          $constraint->aspectRatio();
        });
      }
    }
    
    $img->crop($width, $height);

    if ($img->save($upload)) {
        $result = [
          'status' => 'success',
          'path'  => $upload
        ];
    }
    else {
      $result = [
        'status' => 'fail'
      ];
    }  

    return $result;
  }

  public static function uploadPhoto($foto, $path, $resize=1000, $name=null) {
    // kalo ada foto
    $decoded = base64_decode($foto);

    // cek extension
    $ext = MyHelper::checkExtensionImageBase64($decoded);

    // set picture name
    if($name != null)
      $pictName = $name.$ext;
    else
      $pictName = mt_rand(0, 1000).''.time().''.$ext;
  
    // path
    $upload = $path.$pictName;
    
    $img    = Image::make($decoded);
    
    $width  = $img->width();
    $height = $img->height();


    if($width > 1000){
        $img->resize(1000, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
    } 
    
    $img->resize($resize, null, function ($constraint) {
      $constraint->aspectRatio();
    });
    
    if ($img->save($upload)) {
        $result = [
          'status' => 'success',
          'path'  => $upload
        ];
    }
    else {
      $result = [
        'status' => 'fail'
      ];
    }  

    return $result;
  }
  
  public static function deletePhoto($path) {
    if (file_exists($path)) {
      if (unlink($path)) {
        return true;
      }
      else {
        return false;
      }
    }
    else {
      return true;
    }
  }

}
?>
