<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $plu_id
 * @property string $name
 * @property string $picture
 * @property float $prices
 * @property integer $total
 * @property string $created_at
 * @property string $updated_at
 * @property Voucher[] $vouchers
 */
class Menu extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'menu';

    /**
     * @var array
     */
    protected $fillable = ['plu_id', 'name', 'picture', 'prices', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function vouchers()
    {
        return $this->belongsToMany('App\Http\Models\Voucher', 'win', 'id_menu', 'id_voucher');
    }
}
