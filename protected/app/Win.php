<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id_menu
 * @property integer $id_voucher
 * @property Menu $menu
 * @property Voucher $voucher
 */
class Win extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'win';

    /**
     * @var array
     */
    protected $fillable = ['id_menu', 'id_voucher'];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function menu()
    {
        return $this->belongsTo('App\Http\Models\Menu', 'id_menu');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function voucher()
    {
        return $this->belongsTo('App\Http\Models\Voucher', 'id_voucher');
    }
}
