@extends('layouts.app')

@section('pagecss')

@endsection

@section('content')
<!-- START BREADCRUMB -->
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<a href="{{ url('admin') }}">
				Home
			</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Dashboard</span>
		</li>
	</ul>
</div>
<!-- END BREADCRUMB -->
<!-- START PAGE TITLE -->
<h1 class="page-title">Admin Dashboard</h1>
<!-- END PAGE TITLE -->
<div class="row">

</div>

<div class="row">

</div>
@endsection

@section('pagejs1')
<script src="{{ url('assets/global/plugins/counterup/jquery.waypoints.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/global/plugins/counterup/jquery.counterup.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
@endsection

@section('pagejs2')
<script src="{{ url('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
@endsection

@section('pagejs3')

@endsection