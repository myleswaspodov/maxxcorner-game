@extends('layouts.app')

@section('pagecss')
<link href="{{ url('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<!-- START BREADCRUMB -->
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<a href="{{ url('admin') }}">
				Home
			</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Menu</span>
		</li>
	</ul>
</div>
<!-- END BREADCRUMB -->
<!-- START PAGE TITLE -->
<h1 class="page-title">{{ $title }}</h1>
<!-- END PAGE TITLE -->

@include('notifications')

<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption">
      
    </div>
  </div>
    <div class="portlet-body">
      @foreach($listMenu as $suw)
  
      <form id="form" class="form-horizontal" action="{{ url('admin/menu/update') }}/{{ $suw['plu_id'] }}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-body">
          <div class="form-group">
              <label class="col-md-2 control-label">Name <span class="required" aria-required="true"> * </span></label>
              <div class="col-md-5">
                  <div class="input-icon right">
                      <input class="form-control" type="text" name="name" value="{{ $suw['name'] }}" required>
                  </div>
              </div>
          </div>

          <div class="form-group">
              <label class="col-md-2 control-label">PLU ID <span class="required" aria-required="true"> * </span></label>
              <div class="col-md-5">
                  <div class="input-icon right">
                      <input class="form-control" type="text" name="plu_id" value="{{ $suw['plu_id'] }}" required>
                  </div>
              </div>
          </div>

          <div class="form-group">
              <label class="col-md-2 control-label">Prices <span class="required" aria-required="true"> * </span></label>
              <div class="col-md-5">
                  <div class="input-icon right">
                      <input class="form-control" type="text" name="prices" value="{{ $suw['prices'] }}" required>
                  </div>
              </div>
          </div>

          <div class="form-group">
            <label class="col-md-2 control-label">Picture </label>
            <div class="col-md-10">
              <div class="fileinput fileinput-new" data-provides="fileinput">
                  <div class="fileinput-new thumbnail" style="width: 300px; height: 300;">
                      @if (empty($suw['picture']))
                      <img src="{{ url('image/default.png') }}" alt="">
                      @else
                      <img src="{{ $suw['url_picture'] }}" alt="">
                      @endif
                  </div>
                  <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 300px; max-height: 300px;"></div>
                  <div>
                      <span class="btn default btn-file">
                      <span class="fileinput-new"> Select image </span>
                      <span class="fileinput-exists"> Change </span>
                      <input type="file" accept="image/*" name="picture">
                      </span>
                      <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                  </div>
              </div>
              <br>
              <span class="help-inline">Min.resolution 300px</span> 
              <br>
              <span class="help-inline">Max.resolution 1000px</span> 
              <br>
              <span class="help-inline">Square(1:1)</span>
            </div>
          </div>

          <br>
          <div class="form-group">
            <label class="col-md-2 control-label"> </label>
            <div class="col-md-10">
              <input type="hidden" name="id" value="{{ $suw['id'] }}">
              <button type="submit" class="btn btn md green">Update</button>
            </div>
          </div>

        </div>
      </form>
      @endforeach
  
    </div>
  </div>

@endsection

@section('pagejs1')
<script src="{{ url('assets/global/plugins/counterup/jquery.waypoints.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/global/plugins/counterup/jquery.counterup.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
<script src="{{ url('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
@endsection

@section('pagejs2')
<script src="{{ url('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>


@endsection

@section('pagejs3')

@endsection