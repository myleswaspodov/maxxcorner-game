@extends('layouts.app')

@section('pagecss')
<link href="{{ url('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" /> 
<link href="{{ url('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<!-- START BREADCRUMB -->
<div class="page-bar">
	<ul class="page-breadcrumb">
		<li>
			<a href="{{ url('admin') }}">
				Home
			</a>
			<i class="fa fa-circle"></i>
		</li>
		<li>
			<span>Menu</span>
		</li>
	</ul>
</div>
<!-- END BREADCRUMB -->
<!-- START PAGE TITLE -->
<h1 class="page-title">{{ $title }}</h1>
<!-- END PAGE TITLE -->

@include('notifications')

<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			
		</div>
	</div>
	<div class="portlet-body">
        <table  class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_1">
          <thead>
            <tr>
              <th class="all">PLU ID</th>
              <th class="all">Name</th>
              <th class="all">Prices</th>
              <th class="all">Picture</th>
              <th class="all">Actions</th>
            </tr>
          </thead>
          <tbody>
          <?php setlocale(LC_MONETARY, 'id_ID'); ?>
          @if(!empty($listMenu))
            @foreach($listMenu as $key=>$row)
            <tr>
              <td> {{ $row['plu_id'] }} </td>
              <td> {{ $row['name'] }} </td>
              <td> Rp{{ number_format($row['prices'], 2) }} </td>
              @if (empty($row['picture']))
              <td> <center> <img src="{{ url('image/default.png') }}" width="100px" height="100px" alt="{{ $row['name'] }}"> </center> </td>
              @else
              <td> 
              <center> <img src="{{ $row['url_picture'] }}" width="100px" height="100px" alt="{{ $row['name'] }}"> </center> </td>
              @endif
              <td class="noExport">
                <div class="btn-group pull-right">
                  <button class="btn green btn-xs btn-outline dropdown-toggle" data-toggle="dropdown">Actions
                    <i class="fa fa-angle-down"></i>
                  </button>
                  <ul class="dropdown-menu pull-right">
                    <li>
                      <a href="{{ url('admin/menu/update') }}/{{ $row['plu_id'] }}">
                        <i class="fa fa-pencil"></i> Update </a>
                    </li>
                    <li>
                      <a class="delete" data-id="{{ $row['id'] }}">
                        <i class="fa fa-trash"></i> Delete </a>
                    </li>
                  </ul>
                </div>
              </td>
            </tr>
            @endforeach
          @endif
          </tbody>
        </table>
	</div>
</div>

@endsection

@section('pagejs1')
<script src="{{ url('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>

<script src="{{ url('assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
<script src="{{ url('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
<script src="{{ url('assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
@endsection

@section('pagejs2')
<script src="{{ url('assets/pages/scripts/ui-sweetalert.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function() {
  	var token = "<?php echo csrf_token();?>";
    $('#sample_1').dataTable({
        language: {
            aria: {
                sortAscending: ": activate to sort column ascending",
                sortDescending: ": activate to sort column descending"
            },
            emptyTable: "No data available in table",
            info: "Showing _START_ to _END_ of _TOTAL_ entries",
            infoEmpty: "No entries found",
            infoFiltered: "(filtered1 from _MAX_ total entries)",
            lengthMenu: "_MENU_ entries",
            search: "Search:",
            zeroRecords: "No matching records found"
        },
        buttons: [{
            extend: "print",
            className: "btn dark btn-outline",
            exportOptions: {
                 columns: "thead th:not(.noExport)"
            },
        }, {
          extend: "copy",
          className: "btn blue btn-outline",
          exportOptions: {
               columns: "thead th:not(.noExport)"
          },
        },{
          extend: "pdf",
          className: "btn yellow-gold btn-outline",
          exportOptions: {
               columns: "thead th:not(.noExport)"
          },
        }, {
            extend: "excel",
            className: "btn green btn-outline",
            exportOptions: {
                 columns: "thead th:not(.noExport)"
            },
        }, {
            extend: "csv",
            className: "btn purple btn-outline ",
            exportOptions: {
                 columns: "thead th:not(.noExport)"
            },
        }, {
          extend: "colvis",
          className: "btn red",
          exportOptions: {
               columns: "thead th:not(.noExport)"
          },
        }],
        columnDefs: [{
            className: "control",
            orderable: !1,
            targets: 0
        }],
        order: [0, "asc"],
        lengthMenu: [
            [5, 10, 15, 20, -1],
            [5, 10, 15, 20, "All"]
        ],
        pageLength: 10,
        "bPaginate" : true,
        "bInfo" : true,
        dom: "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>"
    }); 

    $('#sample_1').on('click', '.delete', function(){      
      var id     = $(this).data('id');
      var column = $(this).parents('tr');

    	swal({
    	  title: "Are you sure?",
    	  type: "warning",
    	  showCancelButton: true,
    	  confirmButtonColor: "#DD6B55",
    	  confirmButtonText: "Yes, delete it!",
    	  closeOnConfirm: false
    	},
    	function(){
        $.ajax({
            type    : "POST",
            url     : "<?php echo url('admin/menu/delete')?>",
            data    : "_token="+token+"&id="+id,
            success : function(result) {
              console.log(result);
              if (result === "yes") {
                swal({
                    title: "Menu has been deleted",
                    timer: 1500,
                    type: "success",
                    showConfirmButton: false
                });

                $('#sample_1').DataTable().row(column).remove().draw();
              }
              else {
    	          swal("Deleted!", "Menu unsuccessfully deleted.", "error");
              }
            }
        });
    	});
    });

  });
</script>

@endsection

@section('pagejs3')

@endsection