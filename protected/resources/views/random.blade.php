@extends('layouts.game')

@section('pagecss')
<link rel="stylesheet" href="{{ url('css/animate.min.css') }}">
<link rel="stylesheet" href="{{ url('css/fireworks.css') }}">
<link rel="stylesheet" href="{{ url('css/sweetalert.css') }}">
<link rel="stylesheet" href="{{ url('css/font-awesome.css') }}">
@endsection

@section('pagebody')
<!-- <div class="logo-area">
	<center>
		<img src="{{ url('image/logo.png') }}" alt="Logo Maxx Corner" class="image-logo">
	</center>
</div> -->
<div id="space">
	
</div>
@if(count($menus) > 0)
<div class="game-container">

	<div class="row pad-row">
		<div class="col-md-3">
			<div class="boxed" id="box_0">
				<img src="image/default.png" alt="default" class="img-responsive cover">
				<div class="text-name cover">
					<center><p class="color-black">None</p></center>
				</div>
				<ul class="flip" id="flip_0">
					@foreach($menus as $menu)
					<li>
						<img src="{{ url($menu['url_picture']) }}" alt="{{ $menu['name'] }}" class="img-responsive">
						<div class="text-name">
							<center><p class="color-black">{{ $menu['name'] }} </p></center>
						</div>
					</li>
					@endforeach
				</ul>
			</div>
		</div>
		<div class="col-md-3">
			<div class="boxed" id="box_1">
				<img src="image/default.png" alt="default" class="img-responsive cover">
				<div class="text-name cover">
					<center><p class="color-black">None</p></center>
				</div>
				<ul class="flip" id="flip_1">
					@foreach($menus as $menu)
					<li>
						<img src="{{ url($menu['url_picture']) }}" alt="{{ $menu['name'] }}" class="img-responsive">
						<div class="text-name">
							<center><p class="color-black">{{ $menu['name'] }} </p></center>
						</div>
					</li>
					@endforeach
				</ul>
			</div>
		</div>
		<div class="col-md-3">
			<div class="boxed" id="box_2">
				<img src="image/default.png" alt="default" class="img-responsive cover">
				<div class="text-name cover">
					<center><p class="color-black">None</p></center>
				</div>
				<ul class="flip" id="flip_2">
					@foreach($menus as $menu)
					<li>
						<img src="{{ url($menu['url_picture']) }}" alt="{{ $menu['name'] }}" class="img-responsive">
						<div class="text-name">
							<center><p class="color-black">{{ $menu['name'] }} </p></center>
						</div>
					</li>
					@endforeach
				</ul>
			</div>
		</div>
		<div class="col-md-3">
			<div class="boxed" id="box_3">
				<img src="image/default.png" alt="default" class="img-responsive cover">
				<div class="text-name cover">
					<center><p class="color-black">None</p></center>
				</div>
				<ul class="flip" id="flip_3">
					@foreach($menus as $menu)
					<li>
						<img src="{{ url($menu['url_picture']) }}" alt="{{ $menu['name'] }}" class="img-responsive">
						<div class="text-name">
							<center><p class="color-black">{{ $menu['name'] }} </p></center>
						</div>
					</li>
					@endforeach
				</ul>
			</div>
		</div>
	</div>


	<div class="row pad-row" id="form-tebak">
		<div class="col-md-3"></div>
		<div class="col-md-6">
			<h2 class="color-black"><center>Masukkan total nominal dari menu di atas</center></h2>
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<form action="#" method="post" id="tebak-harga">
						<input type="number" class="form-control input-lg" name="tebakan" placeholder="Jawaban Anda" id="tebakan" value="" autofocus>
					</form>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
		<div class="col-md-3"></div>
	</div>
	<div class="row pad-row">
		<center>
			<div id="timer"></div>
		</center>
	</div>
</div>
@else 

<center>
	<div class="table-container">
		<div class="table-content">

			<h1 class="color-black">Please set your menu first!</h1>

		</div>
	</div>
</center>
@endif

@endsection

@section('additionalbody')
<div class='overlay_lost'>
<center>
	<div class="table-container">
		<div class="table-content">
			<h1 class="big color-white"><i class="fa fa-window-close"></i></h1>
			<h2 class="color-white">
				MAAF!
				<br/>
				Jawaban Anda Salah!
			</h2>
			<br>
			<br>
			<a href="{{ url('/') }}" class="btn btn-maxx" autofocus id="back"> Back to Home </a>
		</div>
	</div>
</center>
</div>

<div class='overlay_timeout'>
<center>
	<div class="table-container">
		<div class="table-content">
			<h1 class="big color-white"><i class="fa fa-clock-o"></i></h1>
			<h2 class="color-white">
				MAAF!
				<br/>
				Waktu Anda Habis!
			</h2>
			<br>
			<br>
			<a href="{{ url('/') }}" class="btn btn-maxx" autofocus id="back"> Back to Home </a>
		</div>
	</div>
</center>
</div>

<div class='overlay_win'>
<center>
	<div class="table-container">
		<div class="table-content">
			<h3 class="not-big-enough">SELAMAT ANDA BENAR!</h3>
			<p>Silakan tukarkan voucher Anda ke kasir.</p>
			<br/>
			<br/>
			<a href="{{ url('/') }}" class="btn btn-maxx" autofocus id="back"> Back to Home </a>
		</div>
	</div>
</center>
</div>
<div id="fireworks-template">
	<div id="fw" class="firework"></div>
	<div id="fp" class="fireworkParticle"><img src="image/particles.gif" alt="" /></div>
</div>

<div id="fireContainer"></div>
@endsection

@section('pagejs')
<script src="{{ url('js/jquery.animate-enhanced.min.js') }}"></script>
<script src="{{ url('js/createjs.min.js') }}"></script>
<script src="{{ url('js/soundmanager2-nodebug-jsmin.js') }}"></script>
<script src="{{ url('js/fireworks.js') }}"></script>
<script src="{{ url('js/sweetalert.min.js') }}"></script>
<script>
	$(document).find('li').hide();
	var url = '{{ url("/") }}';
	createjs.Sound.registerSound("sounds/roll.mp3", "roll");
	createjs.Sound.registerSound("sounds/lost.mp3", "lost");
	createjs.Sound.registerSound("sounds/bell.mp3", "bell");
	createjs.Sound.registerSound("sounds/congratulation.mp3", "congratulation");
	createjs.Sound.registerSound("sounds/timeout.mp3", "timeout");

</script>
<script src="{{ url('js/app.js') }}"></script>
@endsection