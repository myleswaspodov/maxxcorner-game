-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 07 Agu 2017 pada 06.55
-- Versi Server: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tebakharga`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `menu`
--

CREATE TABLE `menu` (
  `id` int(10) UNSIGNED NOT NULL,
  `plu_id` char(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prices` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `menu`
--

INSERT INTO `menu` (`id`, `plu_id`, `name`, `picture`, `prices`, `created_at`, `updated_at`) VALUES
(1, 'beverages0001', 'SINGLE ESPRESSO HOT', 'assets/pages/img/menu/4021501839009.jpeg', '12000.00', NULL, '2017-08-04 09:30:10'),
(2, 'beverages0002', 'DOUBLE ESPRESSO HOT', '', '14000.00', NULL, NULL),
(3, 'beverages0003', 'LONG BLACK HOT', '', '14000.00', NULL, NULL),
(4, 'beverages0004', 'LONG BLACK ICE', '', '14000.00', NULL, NULL),
(5, 'beverages0005', 'CARAMEL MACHIATTO HOT', 'assets/pages/img/menu/3491501839866.jpeg', '24000.00', NULL, '2017-08-04 09:44:27'),
(6, 'beverages0006', 'CARAMEL MACHIATTO ICE', 'assets/pages/img/menu/2711501839788.jpeg', '24000.00', NULL, '2017-08-04 09:43:09'),
(7, 'beverages0007', 'LATTE HOT', 'assets/pages/img/menu/8271501839903.jpeg', '19000.00', NULL, '2017-08-04 09:45:04'),
(8, 'beverages0008', 'LATTE ICE', '', '19000.00', NULL, NULL),
(9, 'beverages0009', 'CAPPUCINO HOT', 'assets/pages/img/menu/3651501839844.jpeg', '19000.00', NULL, '2017-08-04 09:44:05'),
(10, 'beverages0010', 'CAPPUCINO ICE', 'assets/pages/img/menu/8441501839109.jpeg', '19000.00', NULL, '2017-08-04 09:31:50'),
(11, 'beverages0011', 'MILO MOCHA HOT', '', '24000.00', NULL, NULL),
(12, 'beverages0012', 'MILO MOCHA ICE', 'assets/pages/img/menu/5691501839818.jpeg', '24000.00', NULL, '2017-08-04 09:43:38'),
(13, 'beverages0013', 'MELAKA LATTE HOT', 'assets/pages/img/menu/991501839975.jpeg', '24000.00', NULL, '2017-08-04 09:46:16'),
(14, 'beverages0014', 'MELAKA LATTE ICE', '', '24000.00', NULL, NULL),
(15, 'beverages0015', 'FLAVOUR LATTE HOT', '', '24000.00', NULL, NULL),
(16, 'beverages0016', 'FLAVOUR LATTE ICE', '', '24000.00', NULL, NULL),
(17, 'beverages0017', 'COFFEE TUBRUK ROBUSTA HOT', 'assets/pages/img/menu/591501840104.jpeg', '10000.00', NULL, '2017-08-04 09:48:25'),
(18, 'beverages0018', 'COFFEE TUBRUK ARABICA HOT', '', '12000.00', NULL, NULL),
(19, 'beverages0019', 'KOPI SUSU HOT', '', '17000.00', NULL, NULL),
(20, 'beverages0020', 'KOPI SUSU ICE', '', '17000.00', NULL, NULL),
(21, 'beverages0021', 'SAIGON DRIP HOT', '', '17000.00', NULL, NULL),
(22, 'beverages0022', 'SAIGON DRIP ICE', '', '17000.00', NULL, NULL),
(23, 'beverages0023', 'ICE COFFEE WITH COFFEE JELLY ICE', 'assets/pages/img/menu/4461501839285.jpeg', '22000.00', NULL, '2017-08-04 09:34:46'),
(24, 'beverages0024', 'MATCHA GREEN TEA LATTE HOT', 'assets/pages/img/menu/1671501839953.jpeg', '24000.00', NULL, '2017-08-04 09:45:54'),
(25, 'beverages0025', 'MATCHA GREEN TEA LATTE ICE', '', '24000.00', NULL, NULL),
(26, 'beverages0026', 'MILO HOT', '', '24000.00', NULL, NULL),
(27, 'beverages0027', 'MILO ICE', '', '24000.00', NULL, NULL),
(28, 'beverages0028', 'BLACK TEA HOT', 'assets/pages/img/menu/4411501840072.jpeg', '14000.00', NULL, '2017-08-04 09:47:53'),
(29, 'beverages0029', 'BLACK TEA ICE', '', '14000.00', NULL, NULL),
(30, 'beverages0030', 'GREEN TEA HOT', 'assets/pages/img/menu/7091501840046.jpeg', '14000.00', NULL, '2017-08-04 09:47:26'),
(31, 'beverages0031', 'GREEN TEA ICE', '', '14000.00', NULL, NULL),
(32, 'beverages0032', 'BLUEBERRY TEA ICE', 'assets/pages/img/menu/2331501839369.jpeg', '19000.00', NULL, '2017-08-04 09:36:10'),
(33, 'beverages0033', 'LYCHEE TEA WITH LYCHEE POPPING BOBBA ICE', 'assets/pages/img/menu/2951501839184.jpeg', '24000.00', NULL, '2017-08-04 09:33:05'),
(34, 'beverages0034', 'PEACH TEA WITH MANGO POPPING BOBBA ICE', 'assets/pages/img/menu/8281501839206.jpeg', '24000.00', NULL, '2017-08-04 09:33:27'),
(35, 'beverages0035', 'MELAKA MILK TEA ICE', 'assets/pages/img/menu/5891501839243.jpeg', '24000.00', NULL, '2017-08-04 09:34:04'),
(36, 'beverages0036', 'PANDAN TEA WITH COCONUT JELLY ICE', 'assets/pages/img/menu/351501839517.jpeg', '24000.00', NULL, '2017-08-04 09:38:38'),
(37, 'beverages0037', 'GRASS JELLY MILK TEA ICE', 'assets/pages/img/menu/6631501839544.jpeg', '24000.00', NULL, '2017-08-04 09:39:04'),
(38, 'beverages0038', 'KUMQUAT GREEN TEA ICE', 'assets/pages/img/menu/8331501839156.jpeg', '24000.00', NULL, '2017-08-04 09:32:37'),
(39, 'beverages0039', 'LEMON TEA ICE', 'assets/pages/img/menu/9311501839128.jpeg', '19000.00', NULL, '2017-08-04 09:32:09'),
(40, 'beverages0040', 'COCONUT COFFEE ICE', 'assets/pages/img/menu/2241501839632.jpeg', '19000.00', NULL, '2017-08-04 09:40:33'),
(41, 'beverages0041', 'AVOCADO COFFEE ICE', '', '34000.00', NULL, NULL),
(42, 'beverages0042', 'CARAMEL COFFEE ICE', '', '34000.00', NULL, NULL),
(43, 'beverages0043', 'MILO MOCHA CHIP ICE', 'assets/pages/img/menu/331501839685.jpeg', '34000.00', NULL, '2017-08-04 09:41:25'),
(44, 'beverages0044', 'COCONUT MILO CREAM ICE', 'assets/pages/img/menu/6741501839726.jpeg', '24000.00', NULL, '2017-08-04 09:42:07'),
(45, 'beverages0045', 'AVOCADO CHOCOLATE ICE', 'assets/pages/img/menu/2601501839491.jpeg', '34000.00', NULL, '2017-08-04 09:38:12'),
(46, 'beverages0046', 'MATCHA ICE', '', '34000.00', NULL, NULL),
(47, 'beverages0047', 'VANILLA ICE', '', '34000.00', NULL, NULL),
(48, 'beverages0048', 'PRISTINE MINERAL WATER ICE', '', '12000.00', NULL, NULL),
(49, 'beverages0049', 'RAUCH APPLE ICE', '', '19000.00', NULL, NULL),
(50, 'beverages0050', 'RAUCH ORANGE ICE', '', '19000.00', NULL, NULL),
(51, 'food0051', 'White Toast w/ 1 Basic Filling', '', '18000.00', NULL, NULL),
(52, 'food0052', 'White Toast w/ 1 Basic Filling, 1 Topping', '', '21000.00', NULL, NULL),
(53, 'food0053', 'White Toast w/ 1 Basic Filling, 2 Topping', '', '24000.00', NULL, NULL),
(54, 'food0054', 'White Toast w/ 2 Basic Filling, 1 Topping', '', '27000.00', NULL, NULL),
(55, 'food0055', 'White Toast w/ 2 Basic Filling, 2 Topping', '', '30000.00', NULL, NULL),
(56, 'food0056', 'Whole Wheat Toast w/ 1 Basic Filling', '', '18000.00', NULL, NULL),
(57, 'food0057', 'Whole Toast w/ 1 Basic Filling, 1 Topping', '', '21000.00', NULL, NULL),
(58, 'food0058', 'Whole Toast w/ 1 Basic Filling, 2 Topping', '', '24000.00', NULL, NULL),
(59, 'food0059', 'Whole Toast w/ 2 Basic Filling, 1 Topping', '', '27000.00', NULL, NULL),
(60, 'food0060', 'Whole Toast w/ 2 Basic Filling, 2 Topping', '', '30000.00', NULL, NULL),
(61, 'food0061', 'Nutella Spread', '', '3000.00', NULL, NULL),
(62, 'food0062', 'Strawberry Jam', '', '3000.00', NULL, NULL),
(63, 'food0063', 'Peanut Butter', '', '3000.00', NULL, NULL),
(64, 'food0064', 'Kaya Spread', '', '3000.00', NULL, NULL),
(65, 'food0065', 'Chocolate Spread', '', '3000.00', NULL, NULL),
(66, 'food0066', 'White Chocolate', '', '3000.00', NULL, NULL),
(67, 'food0067', 'Chocolate Meises', '', '3000.00', NULL, NULL),
(68, 'food0068', 'Banana', '', '3000.00', NULL, NULL),
(69, 'food0069', 'Marshmallow', '', '3000.00', NULL, NULL),
(70, 'food0070', 'Oreo', '', '3000.00', NULL, NULL),
(71, 'food0071', 'Cheese', '', '3000.00', NULL, NULL),
(72, 'food0072', 'Kaya Toast', '', '18000.00', NULL, NULL),
(73, 'food0073', 'Banana Nutella', 'assets/pages/img/menu/721501840595.jpeg', '21000.00', NULL, '2017-08-04 09:56:36'),
(74, 'food0074', 'Smore''s Chocolate Sandwich', 'assets/pages/img/menu/9091501840552.jpeg', '21000.00', NULL, '2017-08-04 09:55:53'),
(75, 'food0075', 'Oreo & White Chocolate Sandwich', 'assets/pages/img/menu/4131501840629.jpeg', '24000.00', NULL, '2017-08-04 09:57:10'),
(76, 'food0076', 'Local Offerings (Assortment)', '', '7000.00', NULL, NULL),
(77, 'food0077', 'Lupis', '', '7000.00', NULL, NULL),
(78, 'food0078', 'Lapis sagu Pandan', '', '5500.00', NULL, NULL),
(79, 'food0079', 'Lapis Sagu Cokelat', '', '5500.00', NULL, NULL),
(80, 'food0080', 'Risol Ragut', '', '5500.00', NULL, NULL),
(81, 'food0081', 'Pastel Ayam', '', '5500.00', NULL, NULL),
(82, 'food0082', 'Singkong Goreng', 'assets/pages/img/menu/2051501840502.jpeg', '16000.00', NULL, '2017-08-04 09:55:03'),
(83, 'food0083', 'Lasagna', 'assets/pages/img/menu/5451501840346.jpeg', '29000.00', NULL, '2017-08-04 09:52:27'),
(84, 'food0084', 'Macaroni Schotel', 'assets/pages/img/menu/1351501840446.jpeg', '29000.00', NULL, '2017-08-04 09:54:07'),
(85, 'food0085', 'Cheeseburger', 'assets/pages/img/menu/9561501840472.jpeg', '24000.00', NULL, '2017-08-04 09:54:33'),
(86, 'food0086', 'Hot dog', 'assets/pages/img/menu/401501840426.jpeg', '24000.00', NULL, '2017-08-04 09:53:46'),
(87, 'food0087', 'Plain Croissant', '', '10000.00', NULL, NULL),
(88, 'food0088', 'Beef Sausage Croissant', 'assets/pages/img/menu/4161501840181.jpeg', '17000.00', NULL, '2017-08-04 09:49:41'),
(89, 'food0089', 'Cinnamon Roll', '', '12000.00', NULL, NULL),
(90, 'food0090', 'Chocolate Croissant', '', '12000.00', NULL, NULL),
(91, 'food0091', 'Cheese Croissant', 'assets/pages/img/menu/8281501840231.jpeg', '12000.00', NULL, '2017-08-04 09:50:32'),
(92, 'food0092', 'Chicken Curry Puff', 'assets/pages/img/menu/4321501840258.jpeg', '17000.00', NULL, '2017-08-04 09:50:59'),
(93, 'food0093', 'Spicy Chicken', '', '24000.00', NULL, NULL),
(94, 'food0094', 'Italian Cabonara', 'assets/pages/img/menu/1511501840294.jpeg', '24000.00', NULL, '2017-08-04 09:51:34'),
(95, 'food0095', 'Chicken Terriyaki', 'assets/pages/img/menu/8131501840382.jpeg', '24000.00', NULL, '2017-08-04 09:53:03');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_07_09_035329_create_tabel_menu', 1),
(4, '2017_07_09_040153_create_tabel_voucher', 1),
(5, '2017_07_09_040420_create_tabel_win', 1),
(6, '2017_07_09_041230_create_tabel_setting', 1),
(7, '2017_07_20_075250_add_categories_voucher', 2),
(9, '2017_07_24_060020_delete_total_menu', 3),
(10, '2017_07_25_094900_add_field_setting_timer', 4),
(11, '2017_07_31_013810_add_setting_tanggal_expired', 5),
(14, '2017_08_01_043434_add_field_setting', 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `setting`
--

CREATE TABLE `setting` (
  `max_1` int(11) DEFAULT NULL,
  `max_2` int(11) DEFAULT NULL,
  `max_3` int(11) DEFAULT NULL,
  `max_4` int(11) DEFAULT NULL,
  `min_1` int(11) DEFAULT NULL,
  `min_2` int(11) DEFAULT NULL,
  `min_3` int(11) DEFAULT NULL,
  `min_4` int(11) DEFAULT NULL,
  `time` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `voucher_expired` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `setting`
--

INSERT INTO `setting` (`max_1`, `max_2`, `max_3`, `max_4`, `min_1`, `min_2`, `min_3`, `min_4`, `time`, `voucher_expired`) VALUES
(1000, 2000, 30000, 4000, 1000, 2000, 30000, 4000, '10', '2017-08-31 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@example.com', '$2y$10$lOho4PGvVXksY9rGaWeqSeCOUq7N7Ib5X6pbpJ1VnmpM2XYKfLB6u', 'Uc2oip3ef33jvSgNpqY571jJTAIDHB4nkrm0ZTo9fP9AzGhC4yAaavupc4Ml', '2017-07-09 20:45:46', '2017-07-09 20:45:46');

-- --------------------------------------------------------

--
-- Struktur dari tabel `voucher`
--

CREATE TABLE `voucher` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` char(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `category` enum('4','3','2','1') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `win`
--

CREATE TABLE `win` (
  `id_voucher` int(10) UNSIGNED NOT NULL,
  `id_menu` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `voucher`
--
ALTER TABLE `voucher`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `win`
--
ALTER TABLE `win`
  ADD KEY `id_voucher_win` (`id_voucher`),
  ADD KEY `id_menu_win` (`id_menu`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `voucher`
--
ALTER TABLE `voucher`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `win`
--
ALTER TABLE `win`
  ADD CONSTRAINT `id_menu_win` FOREIGN KEY (`id_menu`) REFERENCES `menu` (`id`),
  ADD CONSTRAINT `id_voucher_win` FOREIGN KEY (`id_voucher`) REFERENCES `voucher` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
